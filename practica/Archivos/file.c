#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char* argv[]){

    char* something="ESTO LO AGREGUÉ\n";
    char* content=NULL;
    unsigned long size;
    FILE* file;
    const char* filename=NULL;
    filename = "archivo.txt";
    printf("Archivo a leer: %s\n",filename);
    file = fopen(filename,"r+");

    if(NULL == file){
        fprintf(stderr,"Can't open the file\n");
        exit(-1);
    }

    // Me posiciono al principio del archivo
    // SEEK_SET : desde el comienzo del archivo
    // SEEK_END: desde el final del archivo
    fseek(file,0,SEEK_END);
    // Agrego algo al final del archivo
    fprintf(file, "ACA=%s\n",something);
    // Posición actual y me dice el tamaño
    size = ftell(file);
    
    // Ésto o el rewind es lo mismo, vuelven al principio del archivo
    fseek(file,0,SEEK_SET);
    //rewind(file);
    printf("Tamaño del archivo: %lu\n",size);
    content = (char*) malloc(sizeof(char)*(size));
    fread(content,sizeof(char),size,file);
    printf("Contenido del archivo:%s\n",content);
    fclose(file);
}