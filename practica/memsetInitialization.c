#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>

int main(){

    int status;
    struct addrinfo hints;
    struct addrinfo* serverinfo;

    // Me aseguro que la estructura esté vacía, por eso la lleno de cantidad de 0's como bytes
    // tiene el tamaño de la estructura addrinfo
    // O sea, todas las variables de la estructura addrinfo se inicializaran con 0. 
    // Ej: hints.ai_family , hints.ai_socktype, hints.ai_flags  
    memset(&hints,0,sizeof hints);

    printf("Cantidad de bytes de hints: %ld\n", sizeof hints);
    printf("Direccion de memoria donde reside hints: %p\n", &hints);
    printf("Hints.ai_family: %d\n", hints.ai_family);
    printf("Hints.ai_socktype: %d\n", hints.ai_socktype);
    printf("Hints.ai_flags: %d\n", hints.ai_flags);
    
}