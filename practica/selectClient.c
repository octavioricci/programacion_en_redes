#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#define STDIN 0

int main(){
    struct timeval tv;
    fd_set readfds;

    // Va a esperar 2,5 segundos a que algo aparezca por entrada de teclado
    tv.tv_sec=2;
    tv.tv_usec=500000;

    // Macros que manejan el conjunto de descriptores
    FD_ZERO(&readfds); //limpia el conjunto de descriptores
    FD_SET(STDIN,&readfds); //Agrega el descriptor STDIN al conjunto de descritores
                            // readfds (donde se monitorea si hay caracteres para leer)
    
    select(STDIN+1,&readfds,NULL,NULL,&tv);// no me interesa el cojunto de descriptores de
                                        // escritura y excepción
    
    if(FD_ISSET(STDIN,&readfds))
        printf("Una letra fue presionada!\n");
    else
        printf("Time out!\n");
    
    return(0);
    

}