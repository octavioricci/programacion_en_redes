#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
    int a=0;
    const int* ptr=NULL;

    ptr = &a;

    printf("Ingrese un número: ");
    scanf("%d", &a);

    *ptr=100;
    //printf("Ingrese otro número para cambiar el valor a través del puntero: ");
    //scanf("%d", ptr);

    printf("############## VALOR ################\n");
    printf("%d\n", *ptr);
}