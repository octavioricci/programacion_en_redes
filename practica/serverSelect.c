#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <signal.h>

#define MAX_CONNECTIONS 20
#define STDIN 0
#define TRUE 1

int main(int argc, const char* argv[]){
    char buffer[256];
    int status=0;
    int yes = 1;
    int ret=0;
    int serverSocket;   // Socket del server dentro del main
    int fdmax=0;
    struct addrinfo hints;
    struct addrinfo* serverAddrInfo;
    struct timeval tv;

    memset(&hints,0,sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;    // este seteo le va a decir al gettaddrinfo que
                                    // asigne mi ip local, mi localhost a la estructura
                                    // del socket. De ésta forma no tengo que estar
                                    // harcodeando la ip de mi server
    
    

    printf("\n------ Creando Servidor --------\n");
    if(status = (getaddrinfo(NULL,"8080", &hints,&serverAddrInfo)) !=0){
        fprintf(stderr,"[SERVER]-->Hubo un error creando el server: %s\n",gai_strerror(status));
        return(1);
    }
    
    // Creo el endpoint de comunicación, que nos retornará un FD
    printf("\n------ Creando el Socket --------\n");
    serverSocket = socket(serverAddrInfo->ai_family,serverAddrInfo->ai_socktype,serverAddrInfo->ai_protocol);  
    

    // Opciones para seteos del socket
    // Aca lo que seteamos es que el socket vuelva a rehusar la dirección
    // Lo que hace es permitir a otros sockets que que puedan utilizar o bindear
    // éste puerto, a menos que se esté utiliando.
    // Esto soluciona el problema de "Address already in use", cuando restarteamos
    // el server por un crasheo.
    setsockopt(serverSocket,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes));

    // Función que comunmente se utiliza para file locking, pero tiene funciones
    // de seteos de sockets
    // O_NONBLOCK: Setea que el socket no sea bloqueante.
    // retorna 0 para éxito, o -1 para error
    fcntl(serverSocket,F_SETFD,O_NONBLOCK);

    unsigned int bufferSize = 0;
    unsigned int bufferSizeLenght = sizeof(bufferSize);

    // SO_RCVBUF: receive buffer
    // bufferSize para que sea exitoso debe tener valor distinto a 0
    getsockopt(serverSocket,SOL_SOCKET,SO_RCVBUF, &bufferSize,&bufferSizeLenght);
    printf("Received buffer size: %u\n",bufferSize);

    getsockopt(serverSocket,SOL_SOCKET,SO_SNDBUF,&bufferSize,&bufferSizeLenght);
    printf("Send buffer size: %u\n", bufferSize);

    getsockopt(serverSocket,SOL_SOCKET,SO_REUSEADDR,&bufferSize,&bufferSizeLenght);
    printf("Reuseaddr: %u\n",bufferSize);

    ////////// Vinculo ip y puerto al socket
    printf("\n------ Binding --------\n");
    if(bind(serverSocket,serverAddrInfo->ai_addr,serverAddrInfo->ai_addrlen) < 0){
        perror("[SERVER]--> Hubo un error asociando IP y PUERTO al socket\n");
        return(1);
    }

    ////////// Marco el socket como pasivo, o modo escucha
    if ((listen(serverSocket,MAX_CONNECTIONS)) == -1){
        return(-1);
    }

    fd_set readfds; // Genero un set de filedescriptors de lectura
    fd_set tempreadfds; // Genero otro set de filedescriptors de lectura
    fd_set writefds; // Un socket de cliente estará listo para escribir cuando
                    // la conexión este completamente establecida
    fd_set exceptionfds; // Genero un set de filedescriptors para excepciones

    fdmax = serverSocket;

    // Establezco el intervalo de espera que select bloqueará o esperará
    // que se un filedescriptor de los sets esté listo.
    tv.tv_sec = 2;
    tv.tv_usec = 500000;

    FD_ZERO(&readfds); // Inicializo el set de descriptores
    FD_SET(STDIN,&readfds);
    FD_SET(serverSocket,&readfds); // Agrego el socket al set


    while(TRUE){
        tempreadfds = readfds;
        // mínimamente se otorgará el file descriptor 3
        // entonces fdmax = 3 + 1 = 4
        ret = select(fdmax+1,&tempreadfds,NULL,NULL,&tv);
        printf("Select retornó: %d\n",ret);
        sleep(5);

        if(ret == -1){
            perror("Select");
        }
        // en el primer for, irá de 0 a 3
        // recorro el vector readfds (que contiene las conexiones existenes
        // y pregunto si hubieron cambios, o sea, si hay datos para leer
        // 
        for(int fd = 0; fd <= fdmax ; fd++){

            // pregunto si existe el fd dentro del vector tempreadfds
            // O sea, si tenemos datos nuevos, nueva conexión
            if(FD_ISSET(fd,&tempreadfds)){ 
                printf("Evento en el descriptor: %d OK\n",fd);

                // la nueva conexión es con el socket del server ?
                // entonces es porque recibió un nuevo cliente
                
                if(fd == serverSocket){
                    int clientSocketMainFD;
                    struct sockaddr_in clientAddress;
                    int socketLength = sizeof(struct sockaddr_in);
                    if((clientSocketMainFD = accept(serverSocket,(struct sockaddr*)&clientAddress,(socklen_t*)&socketLength))==-1){
                        perror("[SERVER]--> Error al aceptar cliente\n");
                    }
                    else{
                        printf("[SERVER]--> Cliente Aceptado\n");
                    }

                    // Agrega el clientSocketMainFD al setfd principal
                    FD_SET(clientSocketMainFD,&readfds);
                    
                    // Actualiza el valor del máximo FD en caso de serlo
                    if(clientSocketMainFD > fdmax){
                        fdmax = clientSocketMainFD;
                    }


                }
                // Si la nueva conexión no es con el socket del server
                // trata de recibir los datos (asume que es cliente)
                // Es cualquier cosa menos una conexión nueva
                // Porque el fd nunca va a ser 3, entonces no es fd de conexión nueva
                else{
                    memset(&buffer,0,256);
                    // Recibe mensaje. Si el valor de retorno es 0 significa que el cliente 
                    // se cerró, por ende lo elimina de la escucha
                    ret = recv(fd,&buffer,256,0);
                    //ssize_t readed = recv(fd, &buffer, 256, 0);
                    // Si el recv retorna 0 es porque el cliente cerró la conexión
                    // entonces lo que tengo que hacer es quitar ese fd del set readfds 
                    // original , no el temp
                    if(ret == 0){
                        close(fd);
                        FD_CLR(fd,&readfds);
                        printf("[SERVER]--> Socket eliminado:%d\n",fd);
                    }
                    else{
                        printf("[SERVER]--> Socket:%ld, recibido:%s\n",ret,buffer);

                    }
                } // Cierre else

            }// Cierre if(ISSET)
            else{
                printf("[SERVER]-->File Descriptor:%d\n", fd);
            }

        }// FOR
        printf("\n");
    }// while

    return(0);
    

}// Main