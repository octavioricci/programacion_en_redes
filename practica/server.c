#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define MAX_CONNECTIONS 3
#define PORT 9002
#define PROTOCOL 0

void* connection_handler( void* );


int main(){
    
    char server_message[255]="[Server]-->Bienvenido al Web Server\n";
    pthread_t thread_id;
    /////////// Creo socket
    int server_socket;
    server_socket=socket(AF_INET,SOCK_STREAM,PROTOCOL);
    if (server_socket != -1){
        printf("[SERVER]-->Socket creado exitosamente\n");
    }


    // Defino la dirección del server
    struct sockaddr_in server_address;
    
    // Donde reside la variable sin_family?
    server_address.sin_family= AF_INET;
    // htons es una función que formatea orden en que se envían los bytes
    // al formato de network order (tiene que ver con big endian)
    server_address.sin_port = htons(PORT);

    // Que se conecten al server a cualquier interfaz, no solo localhost
    server_address.sin_addr.s_addr = INADDR_ANY;



    ////////// Vinculo ip y puerto al socket
    if (bind(server_socket,(struct sockaddr*) &server_address, sizeof(server_address)) < 0){
        perror("[SERVER]-> No se pudo vincular ip y puerto al socket\n");
        return(1);
    }


    ////////// Marco el socket como pasivo, o modo escucha
    listen(server_socket,MAX_CONNECTIONS);


    ////////// Acepto conexiones pendientes de hacer
    int client_socket;
    struct sockaddr_in client_address;
    int socket_length = sizeof(struct sockaddr_in);

    // El segundo parámetro me indica la direccion del cliente que se conectó
    // El tercer parámetro me indica el tamaño de la estructura de dirección del cliente
    printf("[SERVER]-->Esperado conexiones entrantes...\n");
    while (client_socket = accept(server_socket,(struct sockaddr*)&client_address,(socklen_t*) &socket_length) ){

        if(pthread_create(&thread_id,NULL,connection_handler,(void*)&client_socket) < 0 ){
            perror("[SERVER]--> Error, no se pudo crear el hilo\n");
        }

    }
    if(client_socket < 0){
        perror("[SERVER]-->Error al aceptar conexiones entrantes\n");
        return(1);
    }
    else if(client_socket > 0){
        
    }

}

void* connection_handler (void* param){
    printf("[SERVER]--> Conexión entrante aceptada con cliente\n");
        
}