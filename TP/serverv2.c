#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <signal.h>
#include "definitions.h"

#define MAX_CONNECTIONS 3
#define MSG_LENGTH 30

int startServer(char* serverName, char* ipType, struct addrinfo hints);
void* connectionHandler(void* param);

int main(int argc, char* argv[]){

    Nodo* nodoPtr=NULL;
    pthread_t thread_id; // Identificador del thread
    int serverSocketMainFD; // Devolverá el file descriptor del socket del server
    int clientSocketMainFD;
    
    struct addrinfo hints;
    
    struct sockaddr_in clientAddress;
    int socketLength = sizeof(struct sockaddr_in);
    char clientString[INET6_ADDRSTRLEN];
    
    memset(&hints,0,sizeof hints);   // Inicializo com 0's la estructura addrinfo
    hints.ai_family = AF_INET; // IPv4
    hints.ai_socktype = SOCK_STREAM; // TCP


    if((serverSocketMainFD = startServer("localhost","9002",hints)) < 0){
        printf("[SERVER]--> Error creating socket\n");
    }

    while(1){
        //nodoPtr->clientSocketMainFD
        //clientSocketMainFD // (struct sockaddr*) &clientAddress
        clientSocketMainFD = accept(serverSocketMainFD,(struct sockaddr*)&clientAddress,(socklen_t*)&socketLength);
        //clientSocketMainFD
        if (clientSocketMainFD < 0){
            printf("[SERVER]--> Error accepting connection: %s\n",strerror(errno));
        }
        
        // Obtengo quien es el que me llama (identifico al cliente)
        inet_ntop(clientAddress.sin_family,&(clientAddress.sin_addr),clientString, sizeof(clientString));
        printf("[SERVER]--> ME LLAMÓ %s\n",clientString);
        
        pthread_t worker; // identificador del thread
        pthread_attr_t  workerAttrs;// atributos de creación del hilo (ej: prioridad)

        pthread_attr_init(&workerAttrs); // seteo el identificador al cual le cargo atributos
        pthread_attr_setdetachstate(&workerAttrs,PTHREAD_CREATE_DETACHED); // cuando finaliza el hilo
                                                            // se libera y no espera a la finalización
                                                            // de los demas
        pthread_attr_setschedpolicy(&workerAttrs,SCHED_FIFO);   // primer hilo en entrar, primero en salir

        
        if(pthread_create(&worker,&workerAttrs,connectionHandler,(void*)&clientSocketMainFD) < 0 ){
            perror("[SERVER]--> Error, no se pudo crear el hilo\n");
        }

    }// cierre While(1)

    close(nodoPtr->clientSocketMainFD);

}


    int startServer(char* serverName, char* ipType, struct addrinfo hints){

        int status; 
        int serverSocketFD;
        struct addrinfo* serverResult;
        struct addrinfo* serverAddrInfo = NULL;

        printf("\n------ Creating Server --------\n");
        if ((status = getaddrinfo(serverName,ipType,&hints,&serverResult)) != 0){
            fprintf(stderr,"There was an error creating the server: %s\n",gai_strerror(status));
            return(1);       
        }

        
        // Si todo salió bien, itero sobre todos los resultados del hostname al que se consultó
        // El resultado me lo trae la estructura serverResult
        // currentAddr es con la que voy recorriendo
        // ai_next es la variable de la estructura addrinfo que enlaza a la próxima estructura
        // Ej: Si consulto google, seguramente tenga 2 resultados (dos structuras addrinfo)
        for(struct addrinfo* currentAddr = serverResult; currentAddr !=NULL;currentAddr = currentAddr->ai_next){
            if(currentAddr->ai_family == AF_INET){ // IPv4
                serverAddrInfo=currentAddr;
            }
        }
        if(NULL == serverAddrInfo){
            printf("IP could not be obtained!\n");
            exit(1);
        }
        
        printf("\n------ Creating Socket --------\n");
        
        serverSocketFD = socket(serverAddrInfo->ai_family,serverAddrInfo->ai_socktype,serverAddrInfo->ai_protocol);
        if(serverSocketFD < 0){
            fprintf(stderr,"There was an error creating the socket: %s\n",gai_strerror(serverSocketFD));
            exit(1);
        }

        ////////// Vinculo ip y puerto al socket
        printf("\n------ Binding --------\n");
        if (bind(serverSocketFD,serverAddrInfo->ai_addr, serverAddrInfo->ai_addrlen) < 0){
            perror("[SERVER]-> There was an error binding ip and port to socket\n");
            return(1);
        }

        ////////// Marco el socket como pasivo, o modo escucha
        if ((listen(serverSocketFD,MAX_CONNECTIONS)) == -1){
            return(-1);
        }

        printf("\n------ Server Succesfuly Created --------\n");
        return serverSocketFD;
    }   


    void* connectionHandler(void* param){
        
        char rec[MSG_LENGTH]= "\0";

        Nodo* nodo = (Nodo*) param;
        int socketClient = nodo->clientSocketMainFD;
        int* socketParam = (int*) param;
        //char* clientString = nodo->clientString;
        //struct sockaddr_in clientAddress = nodo->clientAddress;
        //inet_ntop(clientAddress.sin_family,&(clientAddress.sin_addr),clientString, sizeof(clientString));
        //printf("[SERVER]--> ME LLAMÓ %s\n",clientString);
        // Se queda esperando recibir el mensaje del cliente
        sleep(10);
        recv(socketClient, rec, MSG_LENGTH,0);
        
        // Imprimo lo recibido por el cliente
        printf("[SERVER]--> Client Message: %s\n",rec);    
    }



