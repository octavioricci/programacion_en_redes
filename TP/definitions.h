#ifndef _DEFINITIONS_H

#define _DEFINITIONS_H

#include <arpa/inet.h>

typedef struct nodo{
    int clientSocketMainFD;
    char clientString[INET6_ADDRSTRLEN];
    struct sockaddr_in clientAddress;
}Nodo;

#endif

