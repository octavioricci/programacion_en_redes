#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <netdb.h>


#define MSG_LENGTH 30
int startClient(char* serverName, char* ipType,struct addrinfo hints);

int main(){
    int socketClientMainFD;
    char* clientMessage="Hello, i'm the client";
    char rec[MSG_LENGTH]="\0";
    struct addrinfo hints;

    memset(&hints,0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    if((socketClientMainFD = startClient("localhost","9002",hints)) < 0){
        printf("[CLIENT]--> Error creating socket client\n");
    }

    printf("Connecting to Server...\n");

    // Envia mensaje, strlen consigue la longitud del mensaje (no la longitud del array de char). Flag seteado a 0 
    send(socketClientMainFD, clientMessage, strlen(clientMessage), 0);
  
    printf("Mensaje enviado a server\n");
  
    // Finaliza socket
    close(socketClientMainFD);
	
	return 0;

}


int startClient(char* serverName, char* ipType,struct addrinfo hints){
    int status; 
    int clientSocketFD;
    struct addrinfo* clientResult;
    struct addrinfo* clientAddrInfo = NULL;

    if ((status = getaddrinfo(serverName,ipType,&hints,&clientResult)) != 0){
            fprintf(stderr,"There was an error obtaining the server info: %s\n",gai_strerror(status));
            return(1);       
    } 


        // Si todo salió bien, itero sobre todos los resultados del hostname al que se consultó
        // El resultado me lo trae la estructura serverResult
        // ai_next es la variable de la estructura addrinfo que enlaza a la próxima estructura
        // Ej: Si consulto google, seguramente tenga 2 resultados (dos structuras addrinfo)
        for(struct addrinfo* currentAddr = clientResult; currentAddr !=NULL;currentAddr = currentAddr->ai_next){
            if(currentAddr->ai_family == AF_INET){ // IPv4
                clientAddrInfo=currentAddr;
            }
        }
        if(NULL==clientResult){
            printf("IP could not be obtained!\n");
            exit(1);
        }

        printf("\n------ Creating Client Socket --------\n");
        
        clientSocketFD = socket(clientAddrInfo->ai_family,clientAddrInfo->ai_socktype,clientAddrInfo->ai_protocol);
        if(clientSocketFD < 0){
            fprintf(stderr,"There was an error creating the socket: %s\n",gai_strerror(clientSocketFD));
            exit(1);
        }

        // Se comunica con el Server
        printf("Connecting with Server....\n");
	    if (connect(clientSocketFD, clientAddrInfo->ai_addr, clientAddrInfo->ai_addrlen) == -1)
		    return -1;

        
        
	    printf("Finishing connection....\n");
	    return clientSocketFD;



}