#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <netdb.h>

#define MSG_LENGTH 30 // Longitud de mensaje a enviar

int startClient(char* serverName,char* port, struct addrinfo hints);

int main(){
    int clientSocketMainFD;
    char* clientMessage="Hello, i'm the client";
    char rec[MSG_LENGTH]="\0";
    struct addrinfo hints;

    memset(&hints,0,sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    if((clientSocketMainFD = startClient("localhost","8080",hints)) < 0){
        printf("[CLIENT]--> Error creating client socket\n");
    }
    printf("[CLIENT]--> Connecting to Server...\n");

    // Envia mensaje, strlen consigue la longitud del mensaje (no la longitud del array de char). Flag seteado a 0 
    send(clientSocketMainFD,clientMessage,strlen(clientMessage),0);

    printf("[CLIENT]-> Message sent to server\n");

    // Finaliza socket
    close(clientSocketMainFD);
}
    int startClient(char* serverName,char* port,struct addrinfo hints){
    
        int status;
        int clientSocketFD;
        struct addrinfo* clientResult=NULL;
        struct addrinfo* chosenAddrInfo=NULL;

        if((status=getaddrinfo(serverName,port,&hints,&clientResult)) != 0){
            fprintf(stderr,"[CLIENT]-->There was an error obtaining the Server information: %s\n",gai_strerror(status));
            return(-1);
        }

        // Si todo salió bien, itero sobre todos los resultados del hostname al que se consultó
        // El resultado me lo trae la estructura serverResult
        // currentAddr es con la que voy recorriendo
        // ai_next es la variable de la estructura addrinfo que enlaza a la próxima estructura
        // Ej: Si consulto google, seguramente tenga 2 resultados (dos structuras addrinfo)
        for(struct addrinfo* currentAddr = clientResult;currentAddr!=NULL;currentAddr=currentAddr->ai_next){
            if(currentAddr->ai_family == AF_INET){ // Es IPv4
                chosenAddrInfo  = currentAddr;
            }    
        }
        if(NULL == clientResult){
            printf("[CLIENT]-->IP could not be obtained\n");
            exit(1);
        }

        printf("\n------ Creating Client Socket --------\n");
        clientSocketFD = socket(chosenAddrInfo->ai_family,
                                chosenAddrInfo->ai_socktype,
                                chosenAddrInfo->ai_protocol);
        if(clientSocketFD < 0){
            fprintf(stderr,"There was an error creating the socket: %s\n",gai_strerror(clientSocketFD));
            exit(1);
        }

         // Se comunica con el Server
        printf("Connecting with Server....\n");
        if (connect(clientSocketFD,chosenAddrInfo->ai_addr,
                                chosenAddrInfo->ai_addrlen) == -1){
            printf("[CLIENT]-->There was an error connecting to the server\n");
            return(-1);
        }

        printf("[CLIENT]-->Finishing connection....\n");
	    return clientSocketFD;

    }




