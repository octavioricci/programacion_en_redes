#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <signal.h>
#include "threadpool.h"

#define MAX_CONNECTIONS 3
#define MSG_LENGTH 30

int startServer(char* serverName, char* ipType,struct addrinfo hints);
void threadWorker(void*);

int main(int argc, const char* args[]){

    char clientString[INET6_ADDRSTRLEN];
    int serverSocketMainFD; // File Descriptor del socket del server
    int clientSocketMainFD; // File Descriptor del cliente que me contacte
    struct sockaddr_in clientAddress; // Estructura que contiene IP, puerto, etc del cliente
    int socketLength = sizeof(struct sockaddr_in); // Logitud del cliente devuelto por accept
    struct addrinfo hints; // Se utiliza en el búsqueda de direcciones por getaddrinfo()
                        // Ej:  Si hago nslookup en google me devolverá 2 direcciones
                        //      1 de ipv4 y otra ipv6.
                        //      Con addrinfo, filtro por una que quiera yo
    
    // Creo el POOL de THREADS
    // 1er argumento:   cantidad de worker threads que puede atender a la vez
    // 2do argumento:   cantidad de peticiones que puede tener en cola "task queue"
    // 3er argumento:   flag no utilizado
    threadpool_t* pool = threadpool_create(2,5,0);
    memset(&hints,0,sizeof(hints)); // Inicializao en 0 la estructura addrinfo
    hints.ai_family = AF_INET; // IPv4
    hints.ai_socktype = SOCK_STREAM; // TCP

    if( (serverSocketMainFD = startServer("localhost","8080",hints)) < 0 ){
        printf("[SERVER]--> Error creating socket\n");
        return(-1);
    }

    printf("\n------ Accepting Clients.... --------\n");
    while(1){
        clientSocketMainFD = accept(serverSocketMainFD,(struct sockaddr*)&clientAddress,(socklen_t*)&socketLength);
        if(clientSocketMainFD < 0){
            printf("[SERVER]--> Error accepting connection %s\n",strerror(errno));
        }

        // Obtengo quien es el que me llama (identifico al cliente)
        inet_ntop(clientAddress.sin_family,&(clientAddress.sin_addr),clientString, sizeof(clientString));
        printf("[SERVER]--> ME LLAMÓ %s\n",clientString);

        // Creo los threads en el pool
        threadpool_add(pool,&threadWorker,(void*)&clientSocketMainFD,0);


    
    }
}
    int startServer(char* serverName, char* ipType,struct addrinfo hints){
        int status;
        int serverSocketFD;
        struct addrinfo* serverResult=NULL;
        struct addrinfo* chosenServerAddrInfo=NULL;

        printf("\n------ Creating Server --------\n");
        if( (status=getaddrinfo(serverName,ipType,&hints,&serverResult)) !=0 ){
            fprintf(stderr,"There was an error creating the server: %s\n",gai_strerror(status));
            return(1);
        }

        // Si todo salió bien, itero sobre todos los resultados del hostname al que se consultó
        // El resultado me lo trae la estructura serverResult
        // currentAddr es con la que voy recorriendo
        // ai_next es la variable de la estructura addrinfo que enlaza a la próxima estructura
        // Ej: Si consulto google, seguramente tenga 2 resultados (dos structuras addrinfo)
        for(struct addrinfo* currentAddr = serverResult;currentAddr !=NULL;currentAddr=currentAddr->ai_next){
            if(currentAddr->ai_family == AF_INET) { // Si es IPv4
                chosenServerAddrInfo=currentAddr;
            }
        }    
        if(NULL == chosenServerAddrInfo){
            printf("IP could not be obtained!\n");
            exit(1);
        }

        printf("\n------ Creating Socket --------\n");
        serverSocketFD = socket(chosenServerAddrInfo->ai_family,
                                chosenServerAddrInfo->ai_socktype,
                                chosenServerAddrInfo->ai_protocol);
        if(serverSocketFD <0){
            fprintf(stderr,"There was an error creating the socket: %s\n",gai_strerror(serverSocketFD));
            exit(-1);
        }     
        
        ////////// Vinculo ip y puerto al socket
        printf("\n------ Binding --------\n");
        if(bind(serverSocketFD,chosenServerAddrInfo->ai_addr,chosenServerAddrInfo->ai_addrlen) < 0){
            perror("[SERVER]--> There was an error binding IP and PORT to socket\n");
            return(-1);
        }

        ////////// Marco el socket como pasivo, o modo escucha
        printf("\n------ Listening --------\n");
        if((listen(serverSocketFD,MAX_CONNECTIONS)) == -1 ){
            return(-1);
        }

        printf("\n------ Server Succesfuly Created --------\n");
        return serverSocketFD;

    }    


    void threadWorker(void* params){
        char rec[MSG_LENGTH]= "\0"; // Mensaje a recibir
                                    // Inicializo buffer
        // Casteo el puntero de socket cliente de voud a int
        int* clientSocketFDPtr = (int*) params;
        int clientSocketFD = *clientSocketFDPtr;

        // Se queda esperando recibir el mensaje del cliente
        recv(clientSocketFD,rec,MSG_LENGTH,0);

        // Imprimo lo recibido por el cliente
        printf("[SERVER]--> Client Message: %s\n",rec);
        sleep(2);


    }


