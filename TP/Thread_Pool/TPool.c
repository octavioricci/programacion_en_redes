// Compilo: gcc -g threadpool.c TPool.c -o TPool -pthread 
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <netdb.h>
#include <semaphore.h>
#include <pthread.h>
#include "threadpool.h"

void threadWork(void* param){
    sleep(1);
    printf("Atiendo request de clientes....\n");
}


int main(int argc, const char* argv[]){
    
    // Creo el pool de treads
    // 1er argumento:   cantidad de worker threads que puede atender a la vez
    // 2do argumento:   cantidad de peticiones que puede tener en cola "task queue"
    // 3er argumento:   flag no utilizado
    threadpool_t* pool = threadpool_create(4,5,0);


    // Arego tareas que las asignaré a los workers
    threadpool_add(pool,&threadWork,NULL,0);
    threadpool_add(pool,&threadWork,NULL,0);
    threadpool_add(pool,&threadWork,NULL,0);
    threadpool_add(pool,&threadWork,NULL,0);
    threadpool_add(pool,&threadWork,NULL,0);

    printf("Esperando tareas....\n");

    sleep(10);
    threadpool_destroy(pool,0);

    return(0);
}