#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <signal.h>
#include <semaphore.h>
#include "threadpool.h"

#define MAX_CONNECTIONS 3
#define MSG_LENGTH 30
#define SERVER_IMAGE "yVaEl3ero.jpeg"

int startServer(char* serverName, char* ipType,struct addrinfo hints);
void threadWorker(void*);
char* bodyResponse(char* imageFileName, unsigned long* bodyLength);

int main(int argc, const char* args[]){

    char clientString[INET6_ADDRSTRLEN];
    int serverSocketMainFD; // File Descriptor del socket del server
    int clientSocketMainFD; // File Descriptor del cliente que me contacte
    struct sockaddr_in clientAddress; // Estructura que contiene IP, puerto, etc del cliente
    int socketLength = sizeof(struct sockaddr_in); // Logitud del cliente devuelto por accept
    struct addrinfo hints; // Se utiliza en el búsqueda de direcciones por getaddrinfo()
                        // Ej:  Si hago nslookup en google me devolverá 2 direcciones
                        //      1 de ipv4 y otra ipv6.
                        //      Con addrinfo, filtro por una que quiera yo
    
    // Creo el POOL de THREADS
    // 1er argumento:   cantidad de worker threads que puede atender a la vez
    // 2do argumento:   cantidad de peticiones que puede tener en cola "task queue"
    // 3er argumento:   flag no utilizado
    threadpool_t* pool = threadpool_create(2,5,0);
    memset(&hints,0,sizeof(hints)); // Inicializao en 0 la estructura addrinfo
    // Con hints luego filtro en getaddrinfo lo que estoy buscando
    hints.ai_family = AF_INET; // IPv4
    hints.ai_socktype = SOCK_STREAM; // TCP

    if( (serverSocketMainFD = startServer("localhost","8080",hints)) < 0 ){
        printf("[SERVER]--> Error Initializing Server\n");
        return(-1);
    }

    printf("\n------ Accepting Clients.... --------\n");
    while(1){
        clientSocketMainFD = accept(serverSocketMainFD,(struct sockaddr*)&clientAddress,(socklen_t*)&socketLength);
        if(clientSocketMainFD < 0){
            printf("[SERVER]--> Error accepting connection %s\n",strerror(errno));
        }

        // Obtengo quien es el que me llama (identifico al cliente)
        inet_ntop(clientAddress.sin_family,&(clientAddress.sin_addr),clientString, sizeof(clientString));
        printf("[SERVER]--> ME LLAMÓ %s\n",clientString);

        // Creo los threads en el pool
        // Puntero a una funcion que recibe puntero de tipo void y la funcion retorna void
        // function* (void*) void
        // void (*function)(void *) --> (void*)&clientSocketMainFD 
        threadpool_add(pool,&threadWorker,(void*)&clientSocketMainFD,0);
         
         

         // Destruyo el pool
        //threadpool_destroy(pool, 0);

        
    }
    threadpool_destroy(pool, 0);
    return(0);

}
    int startServer(char* serverName, char* ipType,struct addrinfo hints){
        int status;
        int serverSocketFD;
        struct addrinfo* serverResult=NULL;
        struct addrinfo* chosenServerAddrInfo=NULL;

        printf("\n------ Creating Server --------\n");
        if( (status=getaddrinfo(serverName,ipType,&hints,&serverResult)) !=0 ){
            fprintf(stderr,"There was an error creating the server: %s\n",gai_strerror(status));
            return(1);
        }

        // Si todo salió bien, itero sobre todos los resultados del hostname al que se consultó
        // El resultado me lo trae la estructura serverResult
        // currentAddr es con la que voy recorriendo
        // ai_next es la variable de la estructura addrinfo que enlaza a la próxima estructura
        // Ej: Si consulto google, seguramente tenga 2 resultados (dos structuras addrinfo)
        for(struct addrinfo* currentAddr = serverResult;currentAddr !=NULL;currentAddr=currentAddr->ai_next){
            if(currentAddr->ai_family == AF_INET) { // Si es IPv4
                chosenServerAddrInfo=currentAddr;
            }
        }    
        if(NULL == chosenServerAddrInfo){
            printf("IP could not be obtained!\n");
            exit(1);
        }

        printf("\n------ Creating Socket --------\n");
        serverSocketFD = socket(chosenServerAddrInfo->ai_family,
                                chosenServerAddrInfo->ai_socktype,
                                chosenServerAddrInfo->ai_protocol);
        if(serverSocketFD <0){
            fprintf(stderr,"There was an error creating the socket: %s\n",gai_strerror(serverSocketFD));
            exit(-1);
        }     
        
        ////////// Vinculo ip y puerto al socket
        printf("\n------ Binding --------\n");
        if(bind(serverSocketFD,chosenServerAddrInfo->ai_addr,chosenServerAddrInfo->ai_addrlen) < 0){
            perror("[SERVER]--> There was an error binding IP and PORT to socket\n");
            return(-1);
        }

        ////////// Marco el socket como pasivo, o modo escucha
        printf("\n------ Listening --------\n");
        if((listen(serverSocketFD,MAX_CONNECTIONS)) == -1 ){
            return(-1);
        }

        printf("\n------ Server Succesfuly Created --------\n");
        return serverSocketFD;

    }    


    void threadWorker(void* params){
        
        // Casteo el puntero de socket cliente de void a int
        int* clientSocketFDPtr = (int*) params;
        int clientSocketFD = *clientSocketFDPtr;


        //Header
        char* header = "HTTP 1.0 200 OK\r\nContent-Type: image/jpg\r\nContent-Length: %lu\r\nConnection: close\r\n\r\n";
        unsigned long headerLength = strlen(header);

        // Body (de tipo ul, unsigned long);
        unsigned long bodyLength;

        
        // Función que envio que imagen mostrare y y devuelvo
        // tamaño de todo el body (incluyendo imagen)
        char* body = bodyResponse(SERVER_IMAGE,&bodyLength);
        
        // Calculo tamaño total del response 
        unsigned long responseLength = headerLength + bodyLength;

        // Pido memoria para la variable response
        char* response = malloc(responseLength);
        memset(response,0,responseLength); // limpio contenido
        

        
        sprintf(response, header, bodyLength);

                
        // Envio todo el contenido al browser
        // 1er argumento socket
        // 2do argumento contenido
        // 3er argumento logitud del response
        //send(clientSocketFD,response,strlen(response),0);
        send(clientSocketFD,response,strlen(response),0);
        
        // Envio imagen
        send(clientSocketFD,body,bodyLength,0);
        // Imprimo lo recibido por el cliente
        //printf("[SERVER]--> Client Message: %s\n",rec);
        //sleep(2);

        close(clientSocketFD);

    }

    char* bodyResponse(char* imageFileName, unsigned long* bodyLength){
        
        unsigned long size = 0;
        // puntero a archivo, modo lectura
        char* buffer=NULL;
        FILE * file=NULL;
        file = fopen(imageFileName,"r");
        if(NULL == file){
            printf("[HTTP SERVER]--> There was an error opening the file\n");
            exit(-1); 
        }

        // Me posiciono al principio del archivo
        // SEEK_SET : desde el comienzo del archivo
        // SEEK_END: desde el final del archivo
        fseek(file,0,SEEK_END);
        
        // Posición actual y me dice el tamaño
        size = ftell(file);
        
         // Vuelvo al comienzo del archivo
         // Ésto o el rewind es lo mismo
        fseek(file,0,SEEK_SET);
        //rewind(file);

        // Reservo memoria dinámicamente del tamaño 
        // del archivo que obtuve con ftell y lo almaceno
        // en el buffer, que obtendrá la dirección de memoria
        // donde se almacena el contenido del archivo
        buffer = (char*) malloc (sizeof(char) * size);
        memset(buffer,0,size); // limpio
        fread(buffer,sizeof(char),size,file);
        
        
        // envio el contenido del archivo
        // y tamaño
        // en ambos mando el puntero

        *bodyLength = size;
        return buffer;

    }




